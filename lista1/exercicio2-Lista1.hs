
{-
Exercício 2:
    Escreva um script com as definições das funções a seguir, de maneira que:
    i) identifiquem e utilizem, quando necessário, a modularização
    ii) sejam definições genéricas
    iii) use definição local apenas quando necessário ( promovendo a legibilidade do programa)
-}

-- a) No banco imobiliário, tem-se as seguintes cédulas: 500, 100, 50, 10, 5, 1. Implemente um programa em Haskell que calcule a menor quantidade de cédulas a serem pagas para os seguintes valores:
--    i) Uma dívida a ser paga de 2343 unidades monetárias (U.M) do jogo
--    ii) Um aluguel de 1325 U.M
--    iii) Uma aquisição de imóveis no valor de 3456 U.M

cedulas = [1, 5, 10, 50, 100, 500]

_notas n ceds =
  if (length ceds > 0) then div n cedula + _notas (mod n cedula) (init ceds) else 0
    where cedula = (last ceds) 

notas n =
  _notas n cedulas



-- b) Determinar a área de um retângulo de lados a e b.

areaRetangulo a b = a * b 



-- c) Determinar a área de um círculo de raio r.

areaCirculo r = pi*r^2



-- d) Determinar a distância euclideana entre dois pontos no plano.

distancia x1 y1 x2 y2 = sqrt ((x1-x2)^2 + (y1-y2)^2)



-- e) Determinação das raízes de uma equação de segundo grau.

delta a b c = b^2 - 4*a*c

raizesEq2 a b c =
        if dtsqrt > 0 then
            [ (-b+dtsqrt)/(2*a), (-b-dtsqrt)/(2*a) ]
        else if dtsqrt == 0 then
            [ (-b)*(2*a) ]
        else
            [ ]
    where dtsqrt = sqrt (delta a b c)



-- f) determinar a potência de um número elevado a um expoente negativo, usando na descrição o operador ^ e sem utilizar os operadores ^^ e **.

-- o expoente deve ser passado com sinal negativo, caso positivo, fará a potenciação normalmente
potenciaGambiarra b e = if e >= 0 then b^e else 1/b^(-e)



-- g) determinar a temperatura em graus Farenheit, dada a temperatura em graus Celsius.

c2f tc = tc*1.8+32



-- h) determinar o ganho anual de um valor v aplicado em um banco, rendendo 0,5 % ao mês.

ganhoAnual v = ((1+i/100)^12-1)*v
    where i = 0.5



-- i) determinar a área cinza da figura abaixo, dado o raio r do círculo menor. A figura é formada por dois círculos cujos centros estão situados na mesma reta, paralela ao eixo dos x. Identifique quais são os argumentos necessários para a sua função. Utilize na sua solução a função da letra c).

areaCinza r = areaCirculo (r*3/2) - areaCirculo (r)



-- k) Faça uma função que verifique se um ponto de coordenadas x e y pertence ao primeiro ou ao terceiro quadrante do plano cartesiano.

-- a funcao não considera pontos sobre os eixos
quadrante1ou3 x y = x*y > 0



-- l) determinar se um valor real x pertence ao intervalo [a,b], garantindo que o limite inferior do intervalo é sempre menor ou igual ao limite superior do intervalo.

checaIntervalo a b x = if a <= b then a <= x && x <= b else b <= x && x <= a




