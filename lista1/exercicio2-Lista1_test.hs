
pt name value =
    --putStr (unlines [name, (show value)])
    putStrLn (name ++ ": " ++ (show value))

_quadrante1ou3 (x,y) = quadrante1ou3 x y

main = do
    pt "a" ( map notas [2343, 1325, 3456] )
    pt "b" ( areaRetangulo 4 3 )
    pt "c" ( areaCirculo 1, areaCirculo 2 )  -- 3.14, 2.56
    pt "d" ( distancia (-1) (-1) 2 3 )
    pt "e" ( raizesEq2 1 0 (-1), raizesEq2 1 0 (0), raizesEq2 1 0 (1) )
    pt "f" ( potenciaGambiarra 2 2, potenciaGambiarra 2 (-2) )
    pt "g" ( c2f 0, c2f 50, c2f 100, c2f (-17-7/9) )  -- 32, 122, 212, 0, 
    pt "h" ( ganhoAnual 500 ) -- 525.38
    pt "i" ( areaCinza 1 )
    pt "k" ( map _quadrante1ou3 [(2,2),(-2,2),(-2,-2),(2,-2)] )  -- 1o, 2o, 3o, 4o / True, False, True, False
    pt "l" ( checaIntervalo 1 3 2, checaIntervalo 3 1 2, checaIntervalo (-1) (-3) (-2), checaIntervalo (-3) (-1) (-2), checaIntervalo 0 1 0, checaIntervalo 0 1 (-1) )

