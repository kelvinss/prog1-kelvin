
{-
  Exercício 2: Escreva um script com as definições das funções a seguir, de maneira que:
      i) identifique e utilize, quando necessário, a modularização
     ii) sejam definições genéricas
    iii) use definição local apenas quando necessário (promovendo a legibilidade do programa)
     iv) comente seu código sempre que possível
-}



-- a) Dado um número inteiro, verifique se ele pertence ao intervalo (0,100) e é divisível por 3 e por 5.

intervalo_aberto a b x = a < x && x < b
divisivel d x = mod x d == 0

a x =  intervalo_aberto 0 100 x  &&  divisivel 3 x  &&  divisivel 5 x


-- b) O operador || (OU inclusivo), mapeia dois valores booleanos a e b em True quando pelo menos um deles é True e em False caso contrário. Escreva uma função que denominaremos de oux (OU exclusivo) que se assemelha ao OU inclusivo mas que mapeia em False quando ambos valores são True.

-- oux a b = (a || b) && (not a || not b)

-- oux a b = a == (not b)

oux :: Bool -> Bool -> Bool
oux a b = a /= b



-- c) Dados a data inicial e final de um projeto, informe o número total de dias para a sua execução.


-- funções auxiliares
entre a b x = a <= x && x <= b
minmax a b x = min b (max a x)

-- retorna se um ano é bissexto
ehBissexto a = (divisivel 4 a) && not (divisivel 100 a) || (divisivel 400 a)

-- retorna o número de dias num ano
tamanhoDoAno a = if ehBissexto a then 366 else 365

-- retorna o número de dias no mês
--   Bool bs -> ano bissexto
--   Int  m  -> mes (apartir de 1 = Janeiro)
tamanhoDoMes bs 2 = if bs then 29 else 28
tamanhoDoMes bs m = if oux (odd m) (m>7) then 31 else 30


-- calcula o número de dias desde o comeco do ano até a data especificada d/m/a
diasNoAnoAte a m d  | not (entre 1 12 m)    = 0
                    | (entre 1 tmMes) d   = fn (m-1) 0  + d
                    | otherwise           = fn (m-1) 0  + tmMes
    where
        bs = ehBissexto a
        fn = (diasNoAnoAte a)
        tmMes = tamanhoDoMes bs m

-- calcula o intervalo entre dois anos em dias
diasNoIntervaloDeAnos a1 a2 | a2 > a1 = (tamanhoDoAno a1) + diasNoIntervaloDeAnos (a1+1) a2
                            | a2 == a1 = 0
                            | otherwise = -(diasNoIntervaloDeAnos a2 a1)


-- calcula a quantidade de dias entre duas datas a1/m1/a1 e d2/m2/a2

intervaloDias d1 m1 a1 d2 m2 a2 = (diasNoIntervaloDeAnos a1 a2) + (diasNoAnoAte a2 m2 d2) - (diasNoAnoAte a1 m1 d1)




-- d) Dados três comprimentos de lados l1, l2 e l3, verifique se podem formar um triângulo.

-- calcula se a é menor que a soma de b e c
meqsoma a b c = a < b + c
-- calcula se a é maior que a diferenca de b e c
maqdiff a b c = abs (b-c) < a

testeTamLado a b c = meqsoma a b c && maqdiff a b c

ehTriangulo l1 l2 l3 =
    l1 > 0 && l2 > 0 && l3 > 0 &&
    testeTamLado l1 l2 l3 &&
    testeTamLado l2 l1 l3 &&
    testeTamLado l3 l1 l2


-- e) Calcule o maior de três números.

maior x y = if x > y then x else y

maior3 x y z = maior (maior x y) z


-- f) Dados três números inteiros distintos, calcule o quadrado do sucessor do maior número.
-- Obs: Use a primitiva succ para calcular o sucessor de um número.

f a b c = (succ (maior3 a b c))^2


{-
  g) A empresa Lucro Certo decidiu dar a seus funcionários um abono de natal. A gratificação será
  baseada em dois critérios: o número de horas extras trabalhadas e o número de horas que o
  empregado faltou ao trabalho. O critério estabelecido para calcular o prêmio é: subtrair dois terços
  das horas que o empregado faltou de suas horas extras, obtendo um valor que determina o número
  de pontos do funcionário. Faça uma função para calcular o abono de natal para cada funcionário. A
  distribuição do prêmio é feita de acordo com a tabela abaixo:

      Pontos obtidos        Prêmio em R$
      1 a 10                100,00
      11 a 20               200,00
      21 a 30               300,00
      31 a 40               400,00
      A partir de 41        500,00
-}

tabelaPontos pontos | pontos < 1  = 0
                    | pontos < 11 = 100
                    | pontos < 21 = 200
                    | pontos < 31 = 300
                    | pontos < 41 = 400
                    | otherwise = 500

abono extra falta = tabelaPontos (extra - 2/3*falta)


{-
 h) Considere que o preço de uma passagem de avião em um trecho pode variar dependendo da
    idade do passageiro. Pessoas com 60 anos ou mais pagam apenas 60% do preço total. Crianças até
    10 anos pagam 50% e bebês (abaixo de 2 anos) pagam apenas 10%. Faça uma função que tenha
    como entrada o valor total da passagem e a idade do passageiro e produz o valor a ser pago.
-}

calcpreco valor idade | idade < 2 = 0.10 * valor
                      | idade <= 10 = 0.50 * valor
                      | idade >= 60 = 0.60 * valor
                      | otherwise = valor


-- i) Refaça o exemplo de cálculo de equações de 2º grau, calculando apenas as raízes reais da equação e emitindo uma mensagem de texto, caso contrário.

calcDelta a b c = b^2 - 4*a*c

raiz sqrtdt a b i  = ((-b) + i * sqrtdt) / (2*a)

eq2 a b c = if delta < 0
            then "NAO HA' RAIZES"
            else show (fn (1), fn (-1))
    where
      delta = calcDelta a b c
      fn = raiz (sqrt delta) a b


{-
 j) Dada a idade de uma pessoa em segundos e um planeta do sistema solar, calcule qual seria a
    idade relativa dessa pessoa no planeta informado, sabendo que o Período Orbital é o intervalo de
    tempo que o planeta leva para executar uma órbita em torno do Sol (o que é denominado de ano,
    que na Terra tem aproximadamente 365,25 dias). Considere então as informações abaixo para
    outros planetas do sistema solar:
     · Terra -> período orbital: 365.25 dias na Terra, ou 31557600 segundos
     · Mercúrio -> período orbital: 0.2408467 anos na Terra
     · Vênus -> período orbital: 0.61519726 anos na Terra
     · Marte -> período orbital: 1.8808158 anos na Terra
     · Júpiter -> período orbital: 11.862615 anos na Terra
     · Saturno -> período orbital: 29.447498 anos na Terra
     · Urano -> período orbital: 84.016846 anos na Terra
     · Netuno -> período orbital: 164.79132 anos na Terra
-}

{-
Mercurio  -> 1
Venus     -> 2
Terra     -> 3
Marte     -> 4
Jupiter   -> 5
Saturno   -> 6
Urano     -> 7
Netuno    -> 8
-}

taxaPlaneta planeta | planeta == 1  = 1 / 0.2408467
                    | planeta == 2  = 1 / 0.61519726
                    | planeta == 3  = 1 / 1
                    | planeta == 4  = 1 / 1.8808158
                    | planeta == 5  = 1 / 11.862615
                    | planeta == 6  = 1 / 29.447498
                    | planeta == 7  = 1 / 84.016846
                    | planeta == 8  = 1 / 164.79132
                    | otherwise     = 0

idadeRelativaPlanetaria planeta _idade  = (taxaPlaneta planeta) * idade

                                      where idade = _idade / 31557600
