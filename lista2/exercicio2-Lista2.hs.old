
{-
  Exercício 2: Escreva um script com as definições das funções a seguir, de maneira que:
      i) identifique e utilize, quando necessário, a modularização
     ii) sejam definições genéricas
    iii) use definição local apenas quando necessário (promovendo a legibilidade do programa)
     iv) comente seu código sempre que possível
-}


-- a) Dado um número inteiro, verifique se ele pertence ao intervalo (0,100) e é divisível por 3 e por 5.

entre_aberto a b x = a < x && x < b
divisivel d x = mod x d == 0

a x =  entre_aberto 0 100 x  &&  divisivel 3 x  &&  divisivel 5 x


-- b) O operador || (OU inclusivo), mapeia dois valores booleanos a e b em True quando pelo menos um deles é True e em False caso contrário. Escreva uma função que denominaremos de oux (OU exclusivo) que se assemelha ao OU inclusivo mas que mapeia em False quando ambos valores são True.

-- oux a b = (a || b) && (not a || not b)

-- oux :: Bool -> Bool -> Bool
-- oux a b = a /= b

oux a b = a == (not b)


-- c) Dados a data inicial e final de um projeto, informe o número total de dias para a sua execução.

entre a b x = a <= x && x <= b
minmax a b x = min b (max a x)

-- retorna o número de dias no mês começando por m=1 -> Janeiro
diasNoMes bs 2 = if bs then 29 else 28
diasNoMes bs m = if oux impar (m>7) then 31 else 30
    where impar = mod m 2 == 1

-- diasComecoDoAno m d | m < 1 || m >= 13          = 0
--                     | entre 1 (diasNoMes m) d   = diasComecoDoAno (m-1) 0  + d
--                     | otherwise                 = diasComecoDoAno (m-1) 0  + diasNoMes m
--
-- diasRestoMes m d = minmax 0 31 ( (diasNoMes m) - d  )
--
-- diasRestoDoAno m d  | m < 1 || m >= 13          = 0
--                     | entre 1 (diasNoMes m) d   = diasRestoDoAno (m+1) 0  + diasRestoMes m d
--                     | otherwise                 = diasRestoDoAno (m+1) 0  + diasNoMes m


-- calcula o numero de dias desde o comeco do ano ate a data especificada
offsetDiasNoAno bs m d | m < 1 || m >= 13          = 0
                       | entre 1 (diasNoMes m) d   = offsetDiasNoAno (m-1) 0  + d
                       | otherwise                 = offsetDiasNoAno (m-1) 0  + diasNoMes m


intervaloDias d1 m1 a1 d2 m2 a2 =
  | a1 == a2 =
  | a2 > a1
  | otherwise = -1



-- d) Dados três comprimentos de lados l1, l2 e l3, verifique se podem formar um triângulo.

mqsoma a b c = a < b + c

eh_triangulo l1 l2 l3 =
    l1 > 0 && l2 > 0 && l3 > 0 &&
    mqsoma l1 l2 l3 &&
    mqsoma l2 l1 l3 &&
    mqsoma l3 l1 l2


-- e) Calcule o maior de três números.

maior x y = if x > y then x else y
maior3 x y z = maior (maior x y) z


-- f) Dados três números inteiros distintos, calcule o quadrado do sucessor do maior número.
-- Obs: Use a primitiva succ para calcular o sucessor de um número.

f a b c = (succ (maior3 a b c))^2


{-
  g) A empresa Lucro Certo decidiu dar a seus funcionários um abono de natal. A gratificação será
  baseada em dois critérios: o número de horas extras trabalhadas e o número de horas que o
  empregado faltou ao trabalho. O critério estabelecido para calcular o prêmio é: subtrair dois terços
  das horas que o empregado faltou de suas horas extras, obtendo um valor que determina o número
  de pontos do funcionário. Faça uma função para calcular o abono de natal para cada funcionário. A
  distribuição do prêmio é feita de acordo com a tabela abaixo:

      Pontos obtidos        Prêmio em R$
      1 a 10                100,00
      11 a 20               200,00
      21 a 30               300,00
      31 a 40               400,00
      A partir de 41        500,00
-}

abono extra falta | df < 1  = 0
                  | df < 11 = 100
                  | df < 21 = 200
                  | df < 31 = 300
                  | df < 41 = 400
                  | otherwise = 500

    where df = extra - 2/3*falta


{-
  h) Considere que o preço de uma passagem de avião em um trecho pode variar dependendo da
  idade do passageiro. Pessoas com 60 anos ou mais pagam apenas 60% do preço total. Crianças até
  10 anos pagam 50% e bebês (abaixo de 2 anos) pagam apenas 10%. Faça uma função que tenha
  como entrada o valor total da passagem e a idade do passageiro e produz o valor a ser pago.
-}

calcpreco valor idade | idade < 2 = 0.10 * valor
                      | idade <= 10 = 0.50 * valor
                      | idade >= 60 = 0.60 * valor
                      | otherwise = valor


-- i) Refaça o exemplo de cálculo de equações de 2º grau, calculando apenas as raízes reais da equação e emitindo uma mensagem de texto, caso contrário.

delta a b c = b^2 - 4*a*c
sqrtdelta a b c = sqrt (delta a b c)

eq2r1 a b c = ((-b) + (sqrtdelta a b c)) / (2*a)
eq2r2 a b c = ((-b) + (sqrtdelta a b c)) / (2*a)

eq2 a b c = if dt < 0
            then "NAO HA' RAIZES"
            else show [(eq2r1 a b c),(eq2r2 a b c)]

    where dt = delta a b c


{-
j) Dada a idade de uma pessoa em segundos e um planeta do sistema solar, calcule qual seria a
idade relativa dessa pessoa no planeta informado, sabendo que o Período Orbital é o intervalo de
tempo que o planeta leva para executar uma órbita em torno do Sol (o que é denominado de ano,
que na Terra tem aproximadamente 365,25 dias). Considere então as informações abaixo para
outros planetas do sistema solar:
 · Terra -> período orbital: 365.25 dias na Terra, ou 31557600 segundos
 · Mercúrio -> período orbital: 0.2408467 anos na Terra
 · Vênus -> período orbital: 0.61519726 anos na Terra
 · Marte -> período orbital: 1.8808158 anos na Terra
 · Júpiter -> período orbital: 11.862615 anos na Terra
 · Saturno -> período orbital: 29.447498 anos na Terra
 · Urano -> período orbital: 84.016846 anos na Terra
 · Netuno -> período orbital: 164.79132 anos na Terra
-}

{-
Mercurio  -> 0
Venus     -> 1
Terra     -> 2
Marte     -> 3
Jupiter   -> 4
Saturno   -> 5
Urano     -> 6
Netuno    -> 7
-}

idadeRelativaPlanetaria planeta idade   | planeta == 1  = idade / 0.2408467
                       		        | planeta == 2  = idade / 0.61519726
                                        | planeta == 3  = idade / 1
                                        | planeta == 4  = idade / 1.8808158
                                        | planeta == 5  = idade / 11.862615
                                        | planeta == 6  = idade / 29.447498
                                        | planeta == 7  = idade / 84.016846
                                        | planeta == 8  = idade / 164.79132
                                        | otherwise     = -1



-- testes

main = do
  print (oux True True)
  print (oux True False)
  print (oux False True)
  print (oux False False)
