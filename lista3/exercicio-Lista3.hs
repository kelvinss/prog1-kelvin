
--  Lista 3 --

entre a b x = a <= x && x <= b
media a b = (a+b)/2



-- a) Dado um ponto P(x,y) do plano cartesiano, defina funções que descrevam a sua pertinência nas regiões abaixo:


-- i) Um retângulo (com lados paralelos aos eixos cartesianos) dado pelas coordenadas do canto superior esquerdo e do canto inferior direito, como mostrado na figura.

pertAoRet (ex, ey) (dx, dy) (x, y) = entre dy ey y && entre ex dx x



-- ii) Um losango , com os seus eixos paralelos aos eixos cartesianos, dado pelas coordenadas do vértice esquerdo e do vértice superior, como mostrado na figura

vSum (x1,y1) (x2,y2) = (x1+x2, y1+y2)
vInv (x,y) = (-x,-y)
vSub a b = vSum a (vInv b)
vAbs (x,y) = (abs x, abs y)
calcCentro (x1,y1) (x2,y2) = ((media x1 x2),(media y1 y2))

pertLosango (ex, ey) (sx, sy) (x, y) =
    nx+ny <= 1                          -- ((cx,cy),(tx, ty),(dx, dy),(nx, ny))
  where
    (cx, cy) = (sx, ey)                 -- centro do losando
    (tx, ty) = (abs(ex-cx), abs(sy-cy)) -- tamanho do losango
    (dx, dy) = vSub (x,y) (cx, cy)      -- distancia do ponto ao centro
    (nx, ny) = vAbs (dx/tx, dy/ty)      -- vetor distancia absoluto
                                        --   normalizado pelo tamanho do losango



-- iii) Um círculo dado pelo seu centro C e seu raio r

-- pulei




-- b) Defina funções que implementem as seguintes especificações:
{-
  i) Dados três números a, b e c. Determine quais das seguintes relações eles mantém entre si:
    a) os três são diferentes;
    b) apenas dois são iguais;
    c) os três são iguais.
-}

relacoesDeIgualdade a b c
    | a /= b && b /= c  = 'a'
    | a == b && b == c  = 'c'
    | otherwise         = 'b'


{-
 ii) Dados o canto superior esquerdo e o canto inferior direito de um retângulo R, paralelo aos eixos, determine quantos quadrantes são cobertos por ele.
-}

sinalEhDiferente a b = signum a /= signum b

nDeQuadDoRet (ex,ey) (dx,dy)
    | sinalXmuda && sinalYmuda  = 4
    | sinalXmuda || sinalYmuda  = 2
    | otherwise                 = 1
  where
    sinalXmuda = sinalEhDiferente ex dx   -- calcula se os sinais das componentes x são diferentes
    sinalYmuda = sinalEhDiferente ey dy   -- idem para as componentes y


{-
  c) Calcule o IMC dados o peso em Kg e a altura em m de uma pessoa e retorne o resultado
  em uma mensagem de texto. Verifique como é feito o cálculo do IMC e qual a classificação
  adotada.
-}

imc peso altura
    | i < 18.5  = "Abaixo do peso"
    | i < 25    = "Peso normal"
    | i < 30    = "Sobrepeso"
    | i < 35    = "Obesidade grau I"
    | i < 40    = "Obesidade grau II"
    | otherwise = "Obesidade mórbida"

  where i = peso / altura^2


{-
  d) Calcule o desconto do imposto de renda, segundo a tabela, dado o total de ganhos de
  uma pessoa em reais.
-}

calculoDoRoubo sal
    | sal <= 500  = 0
    | sal <= 1500 = f 0.10
    | sal <= 2500 = f 0.15
    | otherwise   = f 0.25
  where f i = i*sal


{-
  e) Dado três pontos no plano cartesiano, determine se o triângulo formado por estes pontos é equilátero, isoscéles ou escaleno.
-}

ehLado a b x =  abs(a-b) < x  &&  x < a+b

ehTriangulo l1 l2 l3 =
    ehLado l1 l2 l3 &&
    ehLado l2 l1 l3 &&
    ehLado l3 l1 l2

distP2 (x1,y1) (x2,y2) = sqrt ((x2-x1)^2 + (y2-y1)^2)

tipoDeTriangulo (x1,y1) (x2,y2) (x3,y3)
    | not (ehTriangulo l1 l2 l3)  = "Não é triângulo"
    | l1 == l2 && l2 == l3        = "Triângulo equilátero"
    | l1 /= l2 && l2 /= l3        = "Triângulo escaleno"
    | otherwise                   = "Triângulo isóceles"

  where
    l1 = distP2 (x1,y1) (x2,y2)
    l2 = distP2 (x2,y2) (x3,y3)
    l3 = distP2 (x3,y3) (x1,y1)


-- PS.: provavelmente não funciona corretamente devido a arredondamento  -- TODO


{-
  f) Considerando um tabuleiro de xadrez convencional (8x8) e a posição de duas rainhas neste tabuleiro, determine se uma rainha pode capturar a outra.
-}

-- calcula se duas peças estão alinhadas na horizontal ou na vertical
capturaHorVert (x1, y1) (x2, y2) = x1 == x2 || y1 == y2

-- calcula se duas peças estão alinhadas em uma diagonal (i = ±1)
capturaDiagonal_ i (x1, y1) (x2, y2) =  i * (y2-y1) == (x2-x1)

-- calcula se duas peças estão alinhadas em alguma das duas diagonais
capturaDiagonal (x1, y1) (x2, y2) = capturaDiagonal_ 1 (x1, y1) (x2, y2) || capturaDiagonal_ (-1) (x1, y1) (x2, y2)

-- calcula se duas peças estão alinhadas diagonal, horizontal ou verticalmente
rainhaCaptura (x1, y1) (x2, y2) = capturaHorVert (x1, y1) (x2, y2) || capturaDiagonal (x1, y1) (x2, y2)
