

entre a b x = a <= x && x <= b
media a b = (a+b)/2

vSum (x1,y1) (x2,y2) = (x1+x2, y1+y2)
vInv (x,y) = (-x,-y)
vSub a b = vSum a (vInv b)
vAbs (x,y) = (abs x, abs y)
calcCentro (x1,y1) (x2,y2) = ((media x1 x2),(media y1 y2))

pertLosango (ex, ey) (sx, sy) (x, y) =
    nx+ny <= 1                          -- ((cx,cy),(tx, ty),(dx, dy),(nx, ny))
  where
    (cx, cy) = (sx, ey)                 -- centro do losando
    (tx, ty) = (abs(ex-cx), abs(sy-cy)) -- tamanho do losango
    (dx, dy) = vSub (x,y) (cx, cy)      -- distancia do ponto ao centro
    (nx, ny) = vAbs (dx/tx, dy/ty)      -- vetor distancia absoluto
                                        --   normalidado pelo tamanho do losango



--  (0,6)                 (10,6)
--            (5,5)
--     (1,3)  (5,3)  (9,3)
--            (5,1)
-- (0,0)                  (10,0)

test = (pertLosango (1,3) (5,5))
testChar i = if test i then "X" else " "

term_size = (80,40)
edge1 = (0,6)
edge2 = (10,0)
-- size = (10,-6)

range a b st = [a,(a+st)..b]

mkGrid ac = range e1 e2 (sz/(ts-1))
  where
    e1 = (ac edge1)
    e2 = (ac edge2)
    sz = (e2 - e1)
    ts = (ac term_size)

x_grid = mkGrid fst
y_grid = mkGrid snd

draw = [ [ testChar (x,y) | x <- x_grid ] ++ ["\n"] | y <- y_grid ]

main = mapM_ (mapM_ putStr) draw
