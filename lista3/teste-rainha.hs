
import System.IO
-- import Data.List

{- TODO
 * utilizar números nas posicoes das peças
 * impedir sobreposição de peças
-}

-- ### --

-- calcula se duas peças estão alinhasdas na horizontal ou na vertical
capturaHorVert (x1, y1) (x2, y2) = x1 == x2 || y1 == y2

capturaDiagonal_ i (x1, y1) (x2, y2) =  i * (y2-y1) == (x2-x1)

capturaDiagonal (x1, y1) (x2, y2) = capturaDiagonal_ 1 (x1, y1) (x2, y2) || capturaDiagonal_ (-1) (x1, y1) (x2, y2)

rainhaCaptura (x1, y1) (x2, y2) = capturaHorVert (x1, y1) (x2, y2) || capturaDiagonal (x1, y1) (x2, y2)



-- util --

xor :: Bool -> Bool -> Bool
xor True = not
xor False = id

limita a b = max a . min b

-- repeatSt :: String -> Int -> String
-- repeatSt string n = concat $ replicate n string
repeatSt ::  Int -> String -> String
repeatSt n string = concat . (replicate n) $ string

repeatList n = concat . (replicate n)

-- ### --

scale = 3   -- TODO
scale_x = 3
scale_y = 2

x_grid = [0..7]
y_grid = [0..7]

control1 = makeControl 'w' 's' 'd' 'a'
control2 = makeControl 'u' 'j' 'k' 'h'


makeControl c1 c2 c3 c4 c
  | c == c1 = ( 0, 1)
  | c == c2 = ( 0,-1)
  | c == c3 = ( 1, 0)
  | c == c4 = (-1, 0)
  | otherwise = (0,0)



repeatScX = repeatSt scale_x
repeatScY = repeatSt scale_y

limitaX = limita 0 7
limitaY = limita 0 7

movePeca (x,y) (dx,dy) = ( limitaX (x+dx) , limitaY (y+dy) )
movePecas = map ( \(pos,delta) -> movePeca pos delta )


casa (x,y) = if xor (odd x) (odd (y+1)) then "·" else " "

-- ehPeca :: Integral a => [(a,a)] -> (a,a) -> Bool
ehPeca pecas (x,y) = (elem (x,y) pecas)

casaPeca pecas (x,y)
    | ehPeca pecas (x,y)  = "X"
    | otherwise           = casa (x,y)

buildLinha getCasa y =
    repeatScY . (++) "\n" . concatMap (repeatScX . getCasaX)  $  x_grid
  where
    getCasaX x = getCasa (x,y)

-- desenha :: Integral a => [(a,a)] -> IO ()
buildDesenho pecas = --print ""
    concat . map linha . reverse $ y_grid
  where
    getCasa = casaPeca pecas
    linha = buildLinha getCasa



clear = putStr "\ESC[2J"
promptCh = do
  putStr ": "
  c <- getChar
  putStrLn ""
  return c

stringCondicao [r1,r2]
    | c   = "Elas se comem  ( ͡° ͜ʖ ͡°)"
    | otherwise = "Elas não se comem  :("
  where
    c = (rainhaCaptura r1 r2)

run rainhas = do
  clear

  putStrLn "CONTROLES:  W-A-S-D U-H-J-K ; [ENTER] PARA FECHAR\n"
  -- print rainhas
  putStrLn . buildDesenho $ rainhas
  putStrLn . stringCondicao $ rainhas

  c <- promptCh
  if c == '\n'
    then do putStrLn "END"
    else do
      let cts = map ($c) [control1, control2]
      let nrainhas = movePecas $ zip rainhas cts
      run nrainhas


main = do
  hSetBuffering stdin NoBuffering
  hSetEcho stdin False
  -- let rainhas = [(3,0), (3,7)]
  run [(3,0), (3,7)]
