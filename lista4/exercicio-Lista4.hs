
-- Exercício 1

-- a) exemplo

-- b)

foo1 = x + y
    where   x = y
            y = 2

-- o where da definição atribui a y o valor 2 e a x o valor 'y', que portanto será 2
-- foo1 então será associado ao valor de x + y, ou seja 2 + 2 == 4


-- c)
{-
foo2 = x + y
    where x = y
        where y = 2
-}

-- esta definição causa erro pois pode existir apenas um where por definição de função
-- para faze-la funcionar devemos retirar o where, deixando-a igual a foo1


-- d)

foo3 x = if( x <= 30 ) then 'D'
        else if( x <= 50 ) then 'C'
        else if( x <= 80 ) then 'B'
        else 'A'

-- a função foo toma um valor numérico x e retorna um caractere de acordo com a
-- faixa de valores em que está x:
--  'A' para valores maiores que 80  ( 80 < x )
--  'B' para valores menores ou iguais que 80 e maiores que 50  ( 50 < x ≤ 80 )
--  'C' para valores menores ou iguais que 50 e maiores que 30  ( 30 < x ≤ 50 )
--  'D' para valores menores ou iguais que 30  ( x ≤ 30 )



-- Exercício 2

-- a) Dados os pontos A (x1, y1), B (x2, y2) e P (x, y), verifique se o ponto P pertence à reta definida pelos dois pontos A e B.

pertenceAReta (x1, y1) (x2, y2) (x, y) = (y - r x) <= 10^^(-4)   -- 10⁻⁴ → margem de erro
    where
        m = (y2-y1)/(x2-x1)
        r x = m*(x-x1) + y1


{- b) Determine, aproximadamente, quantas pedras foram utilizadas para construir a maior pirâmide de
Gize, sabendo que a altura da pirâmide é igual a 138 m; o tamanho do lado da base  é igual a 227 m
e as dimensões da pedra de calcário são 0.9m x 0.8m x 2.0m  -}

predas = ceiling (pirVol / v)
    where
        pirArea = 227^2
        pirH = 138
        pirVol = pirArea/3 * pirH
        v = 0.9 * 0.8 * 2.0


{- c) Faça uma função que, dada a altura de uma árvore, calcule a velocidade que uma maça que se
desprende chega ao solo, considerando a gravidade igual a 9,81 m/s2.
-}

-- calcula o tempo para realizar determinado deslocamento dx com uma aceleracao a
-- sabendo que dx = at²/2 a partir do repouso
tempoDeslocParaAcc a dx = sqrt (2*dx/a)

-- aumento da velocidade em funcao da aceleração e tempo
dv a dt = a * dt

velQueda h = dv a t
    where
        a = 9.81
        t = tempoDeslocParaAcc a h


{- d) Defina os operadores lógicos a seguir:
    i) and : usando 2 parâmetros de entrada, que tenha o mesmo comportamento do operador &&
    iii) notAnd : usando 2 parâmetros de entrada, a negação de and
    v) impl : usando 2 parâmetros de entrada, a operação de implicação lógica (dadas duas sentenças lógicas p e q, a  implicação é representada por p → q)
-}

-- i)
_and a b =  if a then
              if b then True
              else False
            else False

-- iii)
notAnd a b =  if a then
                if b then False
                else True
              else True

-- v)
impl a b =  if b then True
            else
              if a then False
              else True


{-
  ii) or : usando 2 parâmetros de entrada, que tenha o mesmo comportamento do operador ||
  iv) nor : usando 2 parâmetros de entrada, a negação de nor
  vi) equ : usando 2 parâmetros de entrada, verifica se duas sentenças lógicas são iguais
-}

-- ii)
_or a b = if a then
            True
          else
            if b then True
            else False

-- iv)
_nor a b =  if a then False
            else
              if b then False
              else True

-- vi)
equ a b = if a then
            if b then True else False
          else
            if b then False else True



-- e) Com os operadores lógicos definidos no exercício anterior, implemente o
-- argumento Modus Ponens (modusPonens P Q ==> P, P -> Q, Q) da lógica proposicional.

modusPonens p q = (p, (impl p q), q)



{- f)
Escreva uma função que receba as notas de três provas e de um trabalho e verifique se o aluno
foi aprovado ou reprovado.  Considere a média 5, e que a média das provas equivale à 70% da
nota final e o trabalho computacional, a 30% da nota final.  A resposta deve ser uma tupla com as
notas das três provas, do trabalho, a média, e a string dizendo se o aluno foi aprovado ou reprovado.
da disciplina.
-}

situacaoDoAluno p1 p2 p3 t = (p1, p2, p3, t, media, situacao)
    where
        mediaProva = (p1+p2+p3)/3
        media = mediaProva * 0.70 + t * 0.30
        situacao = if media >= 5 then "Aprovado" else "Reprovado"



{- g) Escreva uma função que tenha um par ordenado como argumento e retorne uma tripla com os
componentes do par ordenado e o terceiro componente com o maior dentre os dois primeiros
componentes. -}

maior a b = if a > b then a else b

tupMaior a b = (a, b, maior a b)



-- h) Traduzir em palavras o nome de um mês em uma data informada.
-- Exemplo: traduz (3, 5, 2004) -> (3, “maio”, 2004)

nomeMes m
  | m == 1  = "Janeiro"
  | m == 2  = "Fevereiro"
  | m == 3  = "Março"
  | m == 4  = "Abril"
  | m == 5  = "Maio"
  | m == 6  = "Junho"
  | m == 7  = "Julho"
  | m == 8  = "Agosto"
  | m == 9  = "Setembro"
  | m == 10 = "Outubro"
  | m == 11 = "Novembro"
  | m == 12 = "Dezembro"
  | otherwise = ""

traduzMes (d,m,a) = (d, (nomeMes m), a)



{- j)
Escreva uma função que recebe uma tupla no formato (‘sexo’,altura) e retorna o peso ideal de
uma pessoa de acordo com os dados abaixo:

    Homens: peso = 72. 7 * altura – 58;
    Mulheres: peso = 62. 1 * altura - 44. 7;
-}

pesoIdeal (sexo,altura)
    | sexo == "M"   = 72.7 * altura - 58
    | sexo == "F"   = 62.1 * altura - 44.7
    | otherwise     = -1


-- k)

pess :: Int->(String, Int, Float, Char)
pess x
    |x==1 = ("Lois", 17, 1.60, 'F')
    |x==2 = ("Tiago", 24, 1.85, 'M')
    |x==3 = ("Maria", 67, 1.55, 'F')
    |x==4 = ("Julia", 33, 1.73, 'M')
    |x==5 = ("Golias", 22, 1.93, 'F')
    |x==6 = ("Leia", 28, 1.73, 'F')
    |x==7 = ("Luke", 28, 1.85, 'M')
    |x==8 = ("Frodo", 31, 1.25, 'M')
    |x==9 = ("Jessica", 25, 1.74, 'F')
    |x==10 = ("Bruce", 31, 1.79,'M')
    |otherwise = ("---",0, 0.0, '-')


maisVelho x1 x2
        | i2 > i1   = n2
        | otherwise = n1
    where
        (n1, i1, _, _) = pess x1
        (n2, i2, _, _) = pess x2


{-
  l) Dado um par de valores (um do tipo inteiro e o outro do tipo float) e uma margem de erro (que
  também é do tipo float), escreva uma função que verifique se o valor real fornecido não ultrapassa a
  margem de erro em relação ao valor inteiro.
  Se qualquer dos valores dados como entrada for nulo ou negativo, a resposta da função deve ser False.
    Exemplos:
      (10,10.5) 0.5 deveria retornar True;
      (10,9.7) 0.5 deveria retornar True;
-}

margemDeErro (med,v) margem
  | med <= 0 || v <= 0 || margem <= 0   = False
  | otherwise                           = abs (v - fromIntegral med) <= margem


{-
m) Faça uma função que, dado um número inteiro, verifique se esse número é divisível por 3, 5 e/ou 7.
Se o número for divisível por apenas um deles, sua função deve responder “Possui apenas um divisor no conjunto”.
Se é divisível por 2 deles, deve responder “Possui dois divisores do conjunto”
e se for divisível pelos 3, deve emitir a mensagem “É divisível por todos os elementos do conjunto”.
-}

oux :: Bool -> Bool -> Bool
oux a b = a /= b
oux3 a b c = oux a (oux b c)

divisivelPor d n = mod n d == 0

divisores357 n
    | d3 && d5 && d7              = "É divisível por todos os elementos do conjunto"   -- 3 verdadeiros
    | not d3 && not d5 && not d7  = "Não possui divisores do conjunto"                 -- 3 falsos
    | oux3 d3 d5 d7               = "Possui apenas um divisor no conjunto"             -- numero impar verdadeiro, excluindo 3, então apenas 1 verdadeiro
    | otherwise                   = "Possui dois divisores do conjunto"                -- excluindo todos os outros casos, apenas 2 verdadeiros
  where
    d3 = divisivelPor 3 n
    d5 = divisivelPor 5 n
    d7 = divisivelPor 7 n




--
