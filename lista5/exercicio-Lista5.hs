

-- ## Lista 5 ##

-- Funções úteis

todas :: (a -> Bool) -> [a] -> Bool
todas fn xs = null [ True | x <- xs, not (fn x) ]

filtra fn xs = [ x | x <- xs, fn x ]

contar x xs = length (filter (==x) xs)



-- Exercício 1: Para cada uma das expressões abaixo, faça o que se pede:

-- I) Escreva descrições usando listas, para as seguintes list as constantes:

-- a) múltiplos de 5 maiores que 0 e menores que 80;

l1a = [5,10..75]


-- b) meses de um ano;

l = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]


-- c) número de dias por cada mês de um ano;

xou :: Bool -> Bool -> Bool
xou = (/=)

diasMes =
    [ if i==2
        then 28
        else if xou (i>7) (odd i)
          then 31
          else 30
      | i <- [1..12] ]



-- d) dias da semana;

diasNaSemana = "domingo" : map (++"-feira") ["segunda", "terça", "quarta", "quinta", "sexta" ] ++ ["sábado"]


--e) relação das disciplinas em que você está matriculado.




-- II) Escreva as listas resultantes das descrições abaixo e depois compare com a resposta da avaliação da lista no HUGS:

-- a) [3*5, 4*5+2.. 100 - 5]

-- [15, 22 .. 95]  -- PA com razão 7 de 15 a 95

l2a = [15,22,29,36,43,50,57,64,71,78,85,92]


-- b) [2, 2*2 .. 4 * 5]

-- [2, 4 .. 20]  -- PA com razão 2 de 2 a 20

l2b = [2,4,6,8,10,12,14,16,18,20]


-- c) f x r t = [x, x + r .. t]

-- PA de `x` até `t` com razão `r`

-- [x, x + r, x + 2*r, x + 3*r, …, x + (n-1)*r, x + n*r ]   -- n = (t-x)/r

f x r t = [ x+r*i | i <- [0..n]]
  where n = div (t-x) r

-- ???



-- == Exercĩcio 2 ==
-- resolver utilizando descrição por listas

-- a) Obter o menor valor de uma lista de números.

menor :: Ord a => [a] -> a
menor xs = head [ x | x <- xs, todas (x<=) xs ]


-- b) Dada uma lista xs, fornecer uma dupla contendo o menor e o maior elemento dessa lista.

maior :: Ord a => [a] -> a
maior xs = head [ x | x <- xs, todas (<=x) xs ]

menorMaior xs = (menor xs, maior xs)


-- c) Produzir uma lista dos múltiplos de um dado número n, menores ou iguais a um dado limite lim.
-- Exemplo: g 5 20 -> [5, 10, 15, 20]

multiplosAte n mx = [ n, n*2 .. mx ]

multiplosAte' n mx = [ n*i | i <- [1..(div mx n)] ]


-- d) Dividir uma lista pela metade e apresentar cada uma das partes em uma dupla.
-- Exemplo:  divideLista [1,3,5,8,15] = ([1,3],[5,8,15] )

divideLista xs = (take q xs, drop q xs)
  where
    q = div (length xs) 2


-- e) Duplicar os elementos de uma lista.
-- Exemplo: duplicaLista [1,2,3] -> [1,1,2,2,3,3]

duplica xs = concat [ [x,x] | x <- xs ]

duplica' xs = [ x | x <- xs, y <- [0,1] ]


-- f) Dadas duas listas de elementos distintos, determinar a união delas.

-- uni' xs ys = [ x | x <- xs, not(elem x ys) ] ++ [ y | y <- ys, not(elem y xs) ] ++ intersec xs ys

uni xs ys = xs ++ [ y | y <- ys, notElem y xs ]


-- g) Dadas duas listas de elementos distintos, determinar a interseção delas.

intersec xs ys = [ x | x <- xs, elem x ys ]


-- h) Calcule a distância de Hamming entre dois números inteiros que possuam, cada um, exatamente n algarismos. A distância de Hamming corresponde ao número de algarismos que diferem em suas posições correspondentes.

algarismo n x = mod (div x (10^n)) 10

algarismos n x = [ algarismo i x | i <- [0 .. n-1] ]

distHamming n a b =  length [ True | i <- [0 .. n-1], algs1!!i /= algs2!!i ]
  where
    algs = algarismos n
    algs1 = algs a
    algs2 = algs b


-- i) Dada uma lista l, contendo uma quantidade igual de números inteiros pares e ímpares (em qualquer ordem), defina uma função que, quando avaliada, produz uma lista na qual esses números pares e ímpares encontram-se alternados.
-- Exemplo: alternaLista [10,2,31,45,6,18,5,20,15,19] -> [10,31,2,45,6,5,18,15,20,19]

pares = filtra even
impares = filtra odd

alternaLista xs = [ k | i <- [0 .. ln-1], k <- [a!!i, b!!i] ]
  where
    a = pares xs
    b = impares xs
    ln = min (length a) (length b)


-- j) Implemente as funções take e drop.

take' i xs = [ xs !! i | i <- [0 .. lst] ]
  where
    lst = (min (length xs) i) - 1

drop' i xs = [ xs !! i | i <- [i .. (ln-1)] ]
  where ln = length xs


-- k) Verificar se um caracter dado como entrada é uma letra.

letras = ['a'..'z'] ++ ['A'..'Z']

ehLetra = (`elem` letras)


-- l) Verificar se um caracter dado como entrada é um dígito.

digitos = ['0' .. '9']

ehDigito = (`elem` digitos)


-- m) Verificar se uma cadeia de caracteres é uma palavra (ou seja, é formada apenas de letras).

ehPalavra = todas ehLetra


-- n) Verificar se uma cadeia de caracteres representa um número inteiro positivo (ou seja, a cadeia de caracteres só é formada por dígitos).

ehIntPos = todas ehDigito


-- o) Dada uma cadeia de caracteres, contar o número de ocorrências de vogais, para cada vogal.

vogais = "aeiou"

numeroDeVogais st = [ contar c st | c <- vogais ]

-- numeroDeVogais' st = map (`contar` st) vogais


--
