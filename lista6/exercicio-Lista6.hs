
-- import qualified Data.Set as Set


-- Exercício: resolver usando descrição por listas

-- retorna uma lista de indices de uma lista de n elementos
idxs n = [0..n-1]

dropa n xs = [ xs!!i | i<-[n..(length xs-1)] ]

notNull xs = not (null xs)

mapa fn xs = [ fn x | x <- xs ]

filtra fn xs = [ x | x <- xs, fn x ]

-- conta os elementos que satisfazem ao precicado fn
conta fn xs = length (filtra fn xs)

-- verifica se nenhum elemento satisfaz ao predicado fn
nenhum fn xs = null [ True | x <- xs, fn x ]


-- a) Dadas duas strings xs e ys, verificar se xs é prefixo de ys.

ehPrefixo xs ys
    -- se o comprimeto da string prefixo é maior que da outra, retorna falso
    | length xs > length ys   = False
    -- faz uma lista dos caracteres que são diferentes e vê se é vazia
    | otherwise               = null [ True | i <- (idxs ln), xs!!i /= ys!!i ]

  where ln = length xs


ehPrefixo' xs ys =  take (length xs) ys == xs



-- b) Dadas duas strings xs e ys, verificar se xs é sufixo de ys.

ehSufixo xs ys
    -- se o comprimeto da string sufixo é maior que da outra, retorna falso
    | length xs > length ys   = False
    -- pega os últimos elementos de ys e compara com xs
    | otherwise               = null [ True | i <- (idxs ln), ys!!(ft+i) /= xs!!i ]
  where
    ln = length xs
    ft = length ys - ln


ehSufixo' xs ys = drop (length ys - length xs) ys == xs



-- c) Dadas duas strings xs e ys, verificar se xs é sublista de ys.

ehSublista xs ys = notNull [ n | n<-[0..lst], ehPrefixo xs (dropa n ys) ]
    where
        lst = (length ys) - (length xs)



-- d) Verificar se uma string é um palíndrome (isso acontece quando a string é a mesma quando lida da esquerda para a direita ou da direita para a esquerda).

palindrome xs = null [ ( ) | i <- idxs (div ln 2), xs!!i /= xs!!(ln-1-i) ]
    where ln = length xs


palindrome' xs =  xs == reverse xs

-- semelhante à anterior, porém inverte apena metade da lista
palindrome'' xs =  take q xs  == reverse (drop (q+r) xs)
  where (q,r) = divMod (length xs) 2


-- e) Verificar se os elementos de uma lista são distintos.

-- para cada elemento x da lista, conta todos os elementos que são iguais a x,
-- e verificar se há apenas 1
distintos xs = null [ True | x <- xs, conta (==x) xs /= 1 ]

distintos' xs = nenhum fn xs
  where fn x = conta (==x) xs /= 1


-- -- implementação bizonhamente mais rápida, requer módulo Data.Set
-- distintos'' xs = Set.size (Set.fromList xs) == length xs



-- f) Dada uma lista de elementos, obter a lista dos mesmos elementos, porém com a ordem inversa.

decr n = [ n, n-1 .. 0 ]

inverte xs = [ xs!!i | i <- decr (length xs-1) ]



-- g) Determinar a posição (ou posições) de um elemento x em uma lista xs, se ele ocorre na lista.

posicaoEm xs el = [ i | i <- [0 .. length xs - 1], xs!!i == el  ]



-- h) Dada uma lista de números ordenada, e um valor, inserir esse valor na lista, de maneira que ela pernaneça ordenada. Exemplo: insereOrdenada 8 [-9, -1,2,6,7,14,15,20] => [-9, -1,2,6,7,8,14,15,20]

insereOrd xs el = [ fn i | i <- [0..length xs - 1] ]
    where
        pos = length (filtra (<=el) xs)
        fn i    | i < pos   = xs!!i
                | i > pos   = xs!!i+1
                | otherwise = el



-- i) Dada uma lista de duplas formadas pelos nomes de pessoas e suas respectivas idades, faça uma função que informe o(s) nome(s) da(s) pessoa(s) mais nova(s) desta lista. A resposta deve ser então uma lista de nomes.

ehMenorNum n xs = nenhum (<n) xs

maisNovos xs = [ nome | (nome,idd) <- xs, ehMenorNum idd idades ]
    where
        idades = mapa snd xs



--
