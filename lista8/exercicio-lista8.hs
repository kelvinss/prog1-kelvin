
-- Lista 8

-- (?) :: Bool -> a -> a -> a
-- (?) True  x _ = x
-- (?) False _ y = y

mapa :: (a -> b) -> [a] -> [b]
mapa _ [] = []
mapa fn (x:xs) = fn x : mapa fn xs

filtra :: (a -> Bool) -> [a] -> [a]
filtra fn [] = []
filtra fn (x:xs) = if fn x then x : resto else resto
  where resto = filtra fn xs

qSort [] = []
qSort (pivo:xs) = qSort menores ++ pivo : qSort maiores
  where
    maiores = filter (>=pivo) xs
    menores = filter (<pivo) xs




-- ==== Exercício 1 ====




-- a) Dadas duas strings xs e ys, verificar se xs é prefixo de ys.

ehPrefixo [] _ = True
ehPrefixo _ [] = False
ehPrefixo (x:xs) (y:ys) = x == y && ehPrefixo xs ys


-- b) Dadas duas strings xs e ys, verificar se xs é sufixo de ys.

ehSufixo [] _ = True
ehSufixo _ [] = False
ehSufixo xs ys = ehPrefixo (inverte xs) (inverte ys)


-- c) Dadas duas strings xs e ys, verificar se xs é sublista de ys.

ehSublista [] _ = True
ehSublista _ [] = False
ehSublista xs ys@(_:cauda) = ehPrefixo xs ys || ehSublista xs cauda


-- d) Verificar se uma string é um palíndrome (isso acontece quando a string é a mesma quando lida da
-- esquerda para a direita ou da direita para a esquerda).

-- divide uma lista em duas
divide xs = fn p [] xs
  where
    p = div (length xs) 2
    fn 0 acc ys = (acc,ys)
    fn n acc (y:ys) = fn (n-1) (acc++[y]) ys

-- verifica se uma string é palídrome
palindrome xs = ehPrefixo comeco ota
    where
      (comeco,final) = divide xs
      ota = inverte final


-- e) Verificar se os elementos de uma lista são distintos.

distintos'' [] = True
distintos'' ll = fn x xs
  where
    fn _ [] = True
    fn comp (y:ys) = y /= comp && fn y ys
    (x:xs) = qSort ll


naoPertence _ [] = True
naoPertence k (x:xs) = x /= k && naoPertence k xs

distintos [] = True
distintos (x:xs) = naoPertence x xs && distintos xs



-- f) Dada uma lista de elementos, obter a lista dos mesmos elementos porém com a ordem inversa.

inverte [] = []
inverte (x:xs) = inverte xs ++ [x]


-- g) Determinar a posição (ou posições) de um elemento x em uma lista xs, se ele ocorre na lista.

posicoes k xs = fn [] 0 xs
  where
    fn pos _ [] = pos
    fn pos i (y:ys) = if y == k then i:resto else resto
      where resto = fn pos (i+1) ys


-- h) Dada uma lista de duplas formadas pelos nomes de pessoas e suas respectivas idades,
-- faça uma função que informe  a lista dos nomes das pessoas, ordenada em ordem decrescente pela idade.
-- Use o algoritmo de ordenação quicksort.

qSortOn :: Ord b => (a -> b) -> [a] -> [a]
qSortOn fn [] = []
qSortOn fn (pivo:xs) = st menores ++ pivo : st maiores
  where
    st = qSortOn fn
    comp = (fn pivo)
    maiores = filter ((>= comp) . fn) xs
    menores = filter ((< comp) . fn) xs

maisVelhos :: Integral idd => [(String, idd)] -> [String]
maisVelhos xs = inverte ( mapa fst (qSortOn snd xs) )



-- ==== Exercício 2 ====

{--
            Espetáculos Teatrais
Vários espetáculos estão sendo apresentados em um grande teatro da cidade. Para cada um dos
espetáculos, registra-se o mapa de ocupação da platéia, conforme as vendas dos ingressos. A platéia
está representada por m filas numeradas de 1 a m, sendo que cada fila contém n cadeiras também
numeradas de 1 a n. Considere a seguinte representação para os dados:

Lugar na platéia  — (fila, cadeira), onde 'fila' é representada por um inteiro de 1 a m e 'cadeira', por um inteiro de 1 a n.
Platéia           — Lista de duplas (lugar, situação) sendo que a 'situação' é: 1 para indicar lugar ocupado e 0 para indicar lugar vago.
Teatro            — Lista de duplas (espetáculo, platéia) onde 'espetáculo' é representado por um inteiro de 1 a p.

Escreva um script em Haskell, com funções que resolvam os problemas abaixo.
Nomes para cada uma das funções são sugeridos ao final do enunciado de cada problema.

--}

type Lugar = (Int, Int)
type Plateia = [(Lugar, Int)]
type Tamanho = (Int, Int)
type Teatro = [(Int, Plateia)]

-- 1 - Dada uma platéia pls, descreva a quantidade total de lugares ocupados (totalOcup).

-- contaCom :: (a -> Bool) -> [a] -> Int
contaCom _ [] = 0
contaCom fn (x:xs) = (if fn x then (1+) else (0+)) $ contaCom fn xs

totalOcup :: Plateia -> Tamanho -> Int
totalOcup pls (m,n) = contaCom ((==1) . snd) pls



-- 2 - Dado um lugar lg e uma platéia pls, verifique se o lugar lg está livre (estaLivre).

estaLivre :: Plateia -> Tamanho -> Lugar -> Bool
estaLivre pls (m,n) lg =
    case res of
      []           ->  error "Lugar não encontrado"  -- True?
      ((_,sit):_)  ->  sit == 0
  where res = filtra ((==lg) . fst) pls



-- 3 - Dado um lugar lg e uma platéia pls, verifique se existe algum vizinho lateral de lg que está livre (vizinhoLivre).

entre a b x = a <= x && x <= b

pegaSit

vizinhoLivre pls (_,n) (i,j) = sits
    where
      pos = filtra (entre 0 n) [(i,j-1),(i,j+1)]
      sits = map (estaLivre pls (m,n)) pos



-- 4 - Dada uma fila fl e uma platéia pls, descreva a lista de cadeiras livres da fila fl (cadeirasLivresFila).


-- 5 - Dada uma platéia pls, descreva a lista de cadeiras livres para cada fila (lugLivresFila)


-- 6 - Dada uma platéia pls, descreva a(s) lista(s) com o maior número de cadeiras livres (filaMaxLivre).


-- 7 - Dado um teatro trs e um espetáculo ep, descreva a sua platéia (plateiaEsp).


-- 8 - Dado um teatro trs, um espetáculo ep e uma fila fl, descreva a lista de cadeiras livres da fila fl (cadeirasLivresFilaEsp).



--

main = return ()
