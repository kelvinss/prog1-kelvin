
-- Lista 9

-- a) Dada uma lista de palavras, ordená-la em ordem alfabética.

qSort :: Ord a => [a] -> [a]
qSort [] = []
qSort (pivo:xs) = qSort menores ++ pivo : qSort maiores
  where
    maiores = filter (>=pivo) xs
    menores = filter (<pivo) xs

ordenaPalavras :: [String] -> [String]
ordenaPalavras palavras = qSort palavras


-- b) Dadas strings xs, ys e zs, substituir xs por zs em ys, se xs for sublista de ys, retornando a string ys modificada. Se xs não ocorrer em ys, a sua função deve retornar a própria ys.
-- Exemplo: subst “funcional” “programacao funcional” “procedural” => “programaca procedural”.

-- verifica se uma lista xs é prefixo de ys
ehPrefixo [] _ = True
ehPrefixo _ [] = False
ehPrefixo (x:xs) (y:ys) = (x==y) && ehPrefixo xs ys

subt [] _ _ = error "lista vazia"
subt _ [] _ = []
subt xs ys@(f:resto) zs =
    if ehPrefixo xs ys                      -- se for prefixo
      then zs ++ subt xs (drop tam ys) zs   -- dropa o tamanho da lista xs, adicina zs, e continua o loop no resto
      else f : subt xs resto zs             -- se não for, repete o primeiro caractere e continua o loop no resto
  where
    tam = length xs



-- c) Dadas duas listas xs e ys, ordenadas em ordem crescente, obter a lista ordenada resultante da intercalação de xs e ys. Exemplo: intercala [1,5,8,10] [2,7,9,20,25] => [1,2,5,7,8,9,10,20,25]

intercala [] [] = []
intercala xs [] = xs
intercala [] ys = ys
intercala xss@(x:xs) yss@(y:ys)=
    if x < y
      then x : intercala xs yss
      else y : intercala xss ys



-- d) Dada uma lista de duplas formadas por nomes de alunos e suas respectivas médias de notas obtidas ao longo do curso, obter uma nova lista de duplas, ordenadas em ordem decrescente dessas médias.
-- Exemplo: mediasAlunos [(“Aline Silva”, 5.4), (“Cristiano Gonçalves”, 9.5), (“Joao Peixoto”, 8.6)] => [(“Cristiano Gonçalves”, 9.5), (“Joao Peixoto”, 8.6), (“Aline Silva”, 5.4)]

qSortOn :: Ord b => (a -> b) -> [a] -> [a]
qSortOn fn [] = []
qSortOn fn (pivo:xs) = srt menores ++ pivo : srt maiores
  where
    srt = qSortOn fn
    comp = (fn pivo)
    maiores = filter ((>= comp) . fn) xs
    menores = filter ((< comp) . fn) xs

mediasAlunos :: (Num n, Ord n) => [(String, n)] -> [(String, n)]
mediasAlunos xs = qSortOn ((0-) . snd) xs


-- e) Dada uma lista de listas, ordene a lista de acordo com o tamanho das sublistas.

ordenaPorTamanho :: [[a]] -> [[a]]
ordenaPorTamanho = qSortOn length


-- f) Utilize os algoritmos de ordenação vistos em sala para ordenar as seguintes listas:
-- i) A lista dos 100 primeiros inteiros em ordem inversa;
-- ii) A lista dos 1000 primeiros reais em ordem inversa;
-- iii) A lista dos 50000 primeiros inteiros em ordem inversa.



-- g) Usando a função foldr crie uma função que receba como parâmetro um inteiro n e retorne a soma dos quadrados dos n primeiros números. Ex: sumsq n = 1^2 + 2^2 + ... + n^2

sumsq n = foldr fn 0 [1..n]
  where fn n acc = acc + n^2


-- h) Usando a função foldl crie uma função que receba como parâmetro um inteiro n e retorne o somatório:
-- "soma de i=0 até i=n de (1/i!)"

fat 0 = 1
fat n = n * fat (n-1)

somatorio n = foldl fn 0 [0..n]
  where fn acc n = acc + (1/(fat n))


--
