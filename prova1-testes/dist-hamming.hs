

toTuple [a,b] = (a,b)

distHamming n1 n2 = length . filter (uncurry (/=)) . uncurry zip . toTuple . (map show) $ [n1, n2]


a1 = 11211
a2 = 11117

main = print $ distHamming a1 a2



