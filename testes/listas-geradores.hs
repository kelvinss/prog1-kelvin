
mapeia fn xs = [ fn x | x <- xs ]

filtra fn xs = [ x | x <- xs, fn x ]


todas :: (a -> Bool) -> [a] -> Bool

todas fn xs = null [ True | x <- xs, not (fn x) ]


menor :: Ord a => [a] -> a

menor xs = head [ x | x <- xs, todas (x<=) xs ]




todas2 :: (a -> Bool) -> [a] -> Bool

todas2 fn xs = length [ True | x <- xs,  (fn x) ] == length xs


menor2 :: Ord a => [a] -> a

menor2 xs = head [ x | x <- xs, todas2 (x<=) xs ]



menor' xs = head ( filtra filtro xs )
  where
    filtro x = (todas (x<=) xs)

--filtro = ((`todas` xs) . (<=))



semRepetir :: Eq a => [a] -> [a]
semRepetir st = [ c | i <- [0..lst], let c = st!!i , todas (/=c) (take i st) ]
  where
    lst = length st - 1


-- foldar fn ini xs -- TODO


main :: IO ()
main =
  print $ menor [5,3,7,2,9]
