
import qualified Data.Set as Set
import qualified Data.Map as Map

-- type Elem = Char
type St a = Set.Set a
type Pair a = (a,a)
type Rel a = Set.Set (Pair a)
type MapRel a = Map.Map a (Set.Set a)


-- COMUM

-- diz se ambos elementos do par `(a,b)` pertencem ao domínio `c`
bothIn :: (Ord a) => St a -> Pair a -> Bool
bothIn c (a,b) = elem a c && elem b c

-- filtra apenas os pares de `r` cujos dois elemetos pertencem ao domínio `a`
filterDomain :: (Ord a) => St a -> Rel a -> Rel a
filterDomain a = Set.filter (bothIn a)



-- REFLEXIVA

-- gera um par "identidade"
idPr :: a -> Pair a
idPr x = (x,x)

-- checa se a relação r é reflexiva
ehReflexiva :: (Ord a) => St a -> Rel a -> Bool
ehReflexiva a r = all ((`elem` r) . idPr) a


-- tpEq :: (Ord a) => Pair a -> Bool
-- tpEq = uncurry (==)
-- ehReflexiva a = (Set.isSubsetOf a) . Set.map fst . Set.filter tpEq

-- ehReflexiva a =   (==) a . Set.foldl folder Set.empty
--   where
--     f t = tpEq t && ((`elem` a) . fst) t
--     folder acc n = if f n then Set.insert (fst n) acc else acc



-- SIMÉTRICA

-- inverte a ordem de um par
invert :: Pair a -> Pair a
invert (a,b) = (b,a)

-- diz se a relação `r` é simétrica em `a`
ehSimetrica :: (Ord a) => St a -> Rel a -> Bool
ehSimetrica a r = all ( (`elem` r) . invert ) . filterDomain a $ r



-- TRANSITIVA

-- adiciona um elemento `e` ao Set `s` amazenado na chave `k` do Map `m`
addToMapSet :: (Ord a) => MapRel a -> a -> a -> MapRel a
addToMapSet m k e = Map.insertWith Set.union k s m
  where s = Set.singleton e

-- converte uma relação em Set para uma relação em Map
-- {('a','b'), ('a','c')} -> {'a': {'b','c'}}
relToMap :: (Ord a) => Rel a -> MapRel a
relToMap = Set.foldl folder Map.empty
  where folder acc (k,e) = addToMapSet acc k e

-- converte uma relação em Map para uma relação em Set
-- {'a': {'b','c'}} -> {('a','b'), ('a','c')}
mapToRel :: (Ord a) => MapRel a -> Rel a
mapToRel = Map.foldlWithKey folder Set.empty
  where folder acc k = Set.union acc . Set.map (\v -> (k,v))

-- substitui todas relações `a -> b -> c` por `a -> c`
buildNewMapRel :: (Ord a) => MapRel a -> MapRel a
buildNewMapRel m = Map.map fn m
  where
    pertM = (`Map.member` m)    -- verifica se uma chave pertence a `m`
    getFromM = (Map.!) m        -- obtem um valor de `m`
    joinSets = Set.foldl Set.union Set.empty    -- concatena um Set de Set's
    fn = joinSets . Set.map getFromM . Set.filter pertM

-- checa se uma relação é transitiva
ehTransitiva :: (Ord a) => St a -> Rel a -> Bool  -- Rel a
ehTransitiva a _r =  all (`elem` r) (mapToRel . buildNewMapRel . relToMap $ r)
  where r = filterDomain a _r


-- -- diz se o segundo elemento do primeito par é igual ao primeiro do segundo
-- sndEqFst :: (Eq a) => (Pair a, Pair a) -> Bool
-- sndEqFst ((_,x),(y,_))  = x == y
-- _newPair :: (Pair a, Pair a) -> Pair a
-- _newPair ((x,_),(_,y))   = (x,y)
--
-- -- diz se a relação `r` é trasitiva em `a`  -- versão usando lista
-- ehTransitiva' :: (Ord a) => St a -> Rel a -> Bool
-- ehTransitiva' a _r = all (`elem` r) [ _newPair (x,y) | x <- r, y <- r, sndEqFst (x,y) ]
--   where
--     r = Set.toList . filterDomain a $ _r



-- testes

_a1,_a2 :: St Char
_r1,_r2 :: Rel Char

_a1 = Set.fromList ['a', 'b', 'c', 'd', 'e']

_r1 = Set.fromList [
    ('a','a'), ('b','b'), ('c','c'), ('d','d'), ('e','e') ,

    ('a','b'), ('b','c'), ('a','c') ,  --  a -> b -> c  ;  a -> c

    ('c','d'), ('d','c') ,
    ('a','d'), ('b','d') , ('a', 'e'),

    ('m','n') , ('k','k')  -- not in a
  ]

_a2 = Set.fromList ['a', 'b', 'c', 'd', 'e', 'f']

_r2 = Set.fromList [
    -- ('a','a'), ('b', 'b'), ('c', 'c'), ('d', 'd'), ('e', 'e') ,
    ('a','b'), ('b','c'), ('a','c'),
    ('e','f') , ('f','e') , ('e','e') , ('f', 'f') ,
    ('m','n') , ('n','j')  -- not in a
  ]


main :: IO ()
main = do
    putStrLn $ "1 reflexiva: " ++ show (ehReflexiva _a1 _r1)
    putStrLn $ "1 simetrica: " ++ show (ehSimetrica _a1 _r1)
    putStrLn $ "1 transitiva: " ++ show (ehTransitiva _a1 _r1)

    putStrLn ""
    putStrLn $ "2 transitiva: " ++ show (ehTransitiva _a2 _r2)

    putStrLn ""
    putStrLn $ "Ø em {a} reflexiva: " ++ show (ehReflexiva s vR)
    putStrLn $ "Ø em  Ø  reflexiva: " ++ show (ehReflexiva vA vR)
    putStrLn $ "Ø simetrica: " ++ show (ehSimetrica s vR)
    putStrLn $ "Ø transitiva: " ++ show (ehTransitiva s vR)

  where
    s = Set.singleton 'a'
    vR :: Rel Char
    vR = Set.empty
    vA :: St Char
    vA = Set.empty

--
